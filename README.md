# Git Init Guide

1. Khởi tạo repo local nếu mới up lần đầu:
- git init
2. Add tất cả file trong thư mục lên git:
- git add .
3. Tạo commit message:
- git commit -m "init repository"
4. :
- git remote add origin https://hungnp3@bitbucket.org/hungnp3/android-examples.git
5. Push lên git:
- git push -u origin master 